PREFIX = /usr/local

INSTALL = install
SHELLCHECK = shellcheck

name = $(notdir $(CURDIR))

target_path = $(PREFIX)/bin/$(name)

.PHONY: test
test:
	$(CURDIR)/test.bash

.PHONY: lint
lint:
	$(SHELLCHECK) *.bash

.PHONY: install
install:
	$(INSTALL) -D fgit.bash $(target_path)

include variables.mk
